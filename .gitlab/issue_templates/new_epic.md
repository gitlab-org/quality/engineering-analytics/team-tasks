## Mission

_what is the goal/what is this epic supposed to improve?_


## Context

_Explain context/scope. The aim is to understand why this issue was created in the first place._

* `Stakeholders [if applicable]`
* _What is in the scope, out of scope?_
* _Non-functional requirements?_


## Outcomes/Objectives

_Measurable benefits for our stakeholders_

- [ ] `Objective 1`
- [ ] `Objective 2`

/promote 
